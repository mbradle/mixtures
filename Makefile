#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2018 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate code for the mixtures project.
#//!
#///////////////////////////////////////////////////////////////////////////////

ifndef NUCNET_TARGET
NUCNET_TARGET = ../nucnet-tools-code
endif

ifndef MY_USER_TARGET
MY_USER_TARGET = ../my_user
endif

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk

VENDORDIR = $(NUCNET_TARGET)/vendor
OBJDIR = $(NUCNET_TARGET)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user
BUILD_DIR = $(NUCNET_TARGET)/build

GC = h5c++

FC = gfortran

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(BUILD_DIR)/Makefile.sparse
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR):$(MY_USER_TARGET)

#===============================================================================
# Add local directory to includes.
#===============================================================================

CFLAGS += -I ./ -I $(MY_USER_TARGET)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

#===============================================================================
# my_user.
#===============================================================================

ifdef MY_USER
  CFLAGS += -DMY_USER
endif

#===============================================================================
# Objects.
#===============================================================================

MIXTURES_OBJ = $(WN_OBJ)          \
               $(SP_OBJ)          \
               $(MY_USER_OBJ)     \
               $(NNT_OBJ)         \
               $(SOLVE_OBJ)       \
               $(HD5_OBJ)         \
               $(USER_OBJ)        \
               $(OPTIONS_OBJ)     \
               $(ILU_OBJ)         \

MIXTURES_DEP = $(MIXTURES_OBJ)

CFLAGS += -DSPARSKIT2
MIXTURES_DEP += sparse
FLIBS= -L$(SPARSKITDIR) -lskit
CLIBS += -lgfortran

#===============================================================================
# Executables.
#===============================================================================

MIXTURES_EXEC = diff_mixing

$(MIXTURES_EXEC): $(MIXTURES_DEP)
	$(CC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(CC) $(MIXTURES_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(FLIBS) $(CLIBS)

.PHONY all_mixtures: $(MIXTURES_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_mixtures cleanall_mixtures

clean_mixtures:
	rm -f $(MIXTURES_OBJ)

cleanall_mixtures: clean_mixtures
	rm -f $(BINDIR)/$(MIXTURES_EXEC) $(BINDIR)/$(MIXTURES_EXEC).exe

clean_nucnet:
	rm -fr $(NUCNET_TARGET)
