///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to decay input abundances over input time.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <iostream>
#include <string>

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "nnt/iter.h"

#define S_F   "f"

//##############################################################################
// add_missing_zone().
//##############################################################################

void
add_missing_zone(
  Libnucnet * p_nucnet1,
  Libnucnet * p_nucnet2,
  std::vector<std::string> s_new_zone
)
{

  double d_f_sum = 0;
  nnt::zone_list_t zone_list;
  gsl_vector * p1, * p2;

  Libnucnet__Zone * p_result, * p_new_zone;

  zone_list = nnt::make_zone_list( p_nucnet2 );

  if( zone_list.size() != 1 )
  {
    std::cerr << "Incorrect number of zones." << std::endl;
    exit( EXIT_FAILURE );
  }

  p_result = zone_list.begin()->getNucnetZone();

  p1 = Libnucnet__Zone__getAbundances( p_result ); 

  zone_list = nnt::make_zone_list( p_nucnet1 );

  BOOST_FOREACH( nnt::Zone zone, zone_list )
  {

    double d_f = zone.getProperty<double>( S_F );
    d_f_sum += d_f;
    p2 = Libnucnet__Zone__getAbundances( zone.getNucnetZone() );
    gsl_vector_scale( p2, d_f );
    gsl_vector_sub( p1, p2 );

    gsl_vector_free( p2 );

  }

  if( d_f_sum > 1 )
  {
    std::cerr << "Summed contributions > 1." << std::endl;
    exit( EXIT_FAILURE );
  }

  d_f_sum = 1. - d_f_sum;

  gsl_vector_scale( p1, 1. / d_f_sum );

  if( s_new_zone.size() == 1 )
    p_new_zone =
      Libnucnet__Zone__new(
        Libnucnet__getNet( p_nucnet1 ),
        s_new_zone[0].c_str(),
        NULL,
        NULL
      );
  else if( s_new_zone.size() == 2 )
    p_new_zone =
      Libnucnet__Zone__new(
        Libnucnet__getNet( p_nucnet1 ),
        s_new_zone[0].c_str(),
        s_new_zone[1].c_str(),
        NULL
      );
  else
    p_new_zone =
      Libnucnet__Zone__new(
        Libnucnet__getNet( p_nucnet1 ),
        s_new_zone[0].c_str(),
        s_new_zone[1].c_str(),
        s_new_zone[2].c_str()
      );

  Libnucnet__addZone( p_nucnet1, p_new_zone );

  Libnucnet__Zone__updateProperty(
    p_new_zone,
    S_F,
    NULL,
    NULL, 
    boost::lexical_cast<std::string>( d_f_sum ).c_str()
  );

  Libnucnet__Zone__updateAbundances( p_new_zone, p1 );

  gsl_vector_free( p1 );

}
  
//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{

  Libnucnet * p_nucnet1, * p_nucnet2;
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

  //============================================================================
  // Check input.
  //============================================================================

  if( argc == 2 && strcmp( argv[1], "--example" ) == 0 )
  {
    std::cout << std::endl;
    std::cout << argv[0] << " ../nucnet-tools-code/data_pub/my_net.xml " <<
      "zone.xml param.xml" <<
      std::endl << std::endl;
    exit( EXIT_FAILURE );
  }

  if( argc < 6 || argc > 7 )
  {
    fprintf(
      stderr,
      "\nUsage: %s net_file zones_file result_file new_zone_name output zone_xpath\n\n",
      argv[0]
    );
    fprintf(
      stderr,
      "  net_file = input network xml file\n\n"
    );
    fprintf(
      stderr,
      "  zones_file = zones already present\n\n"
    );
    fprintf(
      stderr,
      "  result_file = resultant\n\n"
    );
    fprintf(
      stderr,
      "  new_zone_name = name for new zone\n\n"
    );
    fprintf(
      stderr,
      "  output = output file name\n\n"
    );
    fprintf(
      stderr,
      "  zone_xpath = XPath to select zones (optional)\n\n"
    );
    std::cout << "For an example usage, type " << std::endl << std::endl;
    std::cout << argv[0] << " --example" << std::endl << std::endl;
    return EXIT_FAILURE;
  }

  //============================================================================
  // Get data.
  //============================================================================

  p_nucnet1 = Libnucnet__new();

  p_nucnet2 = Libnucnet__new();

  Libnucnet__Nuc__updateFromXml(
    Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet1 ) ),
    argv[1],
    ""
  );

  Libnucnet__Nuc__updateFromXml(
    Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet2 ) ),
    argv[1],
    ""
  );

  if( argc == 7 )
    Libnucnet__assignZoneDataFromXml( p_nucnet1, argv[2], argv[5] );
  else
    Libnucnet__assignZoneDataFromXml( p_nucnet1, argv[2], "" );

  Libnucnet__assignZoneDataFromXml( p_nucnet2, argv[3], "" );

  //============================================================================
  // Get label for missing zone.
  //============================================================================

  std::string s_arg = argv[4];
  std::vector<std::string> s_new_zone;

  boost::char_separator<char> sep(",");
  tokenizer tok( s_arg, sep);
  for( tokenizer::iterator it = tok.begin(); it != tok.end(); ++it )
  {
    std::string s_tmp = *it;
    boost::algorithm::trim( s_tmp );
    s_new_zone.push_back( s_tmp );
    if( s_new_zone.size() > 3 )
    {
      std::cerr << "Too many new zone labels." << std::endl;
      return EXIT_FAILURE;
    }
  }

  //============================================================================
  // Add missing zone.
  //============================================================================

  add_missing_zone( p_nucnet1, p_nucnet2, s_new_zone );

  //============================================================================
  // Output.
  //============================================================================

  Libnucnet__writeZoneDataToXmlFile( p_nucnet1, argv[5] );

  //============================================================================
  // Clean up and exit.
  //============================================================================

  Libnucnet__free( p_nucnet1 );
  Libnucnet__free( p_nucnet2 );

  return EXIT_SUCCESS;

}
